#include "postgres_ext.h"
#include "pool_type.h"

// copied from utils/elog.h
#if defined(WIN32) || defined(__CYGWIN__)

#ifdef BUILDING_DLL
#define PGDLLIMPORT __declspec (dllexport)
#else							/* not BUILDING_DLL */
#define PGDLLIMPORT __declspec (dllimport)
#endif

#ifdef _MSC_VER
#define PGDLLEXPORT __declspec (dllexport)
#else
#define PGDLLEXPORT
#endif
#else							/* not CYGWIN, not MSVC, not MingW */
#define PGDLLIMPORT
#define PGDLLEXPORT
#endif
