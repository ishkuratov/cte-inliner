#include "libpq-fe.h"

void
print_pgconn_options(PGconn *conn);

void
print_parsed_pgconn_options(const char *conninfo);

void
print_conn_info_options(PQconninfoOption *opts);
