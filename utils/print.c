#include "utils/print.h"
#include <stdio.h>

void
print_pgconn_options(PGconn *conn)
{
    print_conn_info_options(PQconninfo(conn));
}

void
print_parsed_pgconn_options(const char *conninfo)
{
    char *errmsg = NULL;
    PQconninfoOption *opts = PQconninfoParse(conninfo, &errmsg);
    if (opts == NULL) {
        if (errmsg == NULL) {
            fprintf(stderr, "Out of memory.");
            return;
        }
        fprintf(stderr, "utils:print: %s", errmsg);
        PQfreemem(errmsg);
        return;
    }
    print_conn_info_options(opts);
}

void
print_conn_info_options(PQconninfoOption *opts)
{
    for (PQconninfoOption *opt = opts; opt->keyword; ++opt)
    {
        if (opt->val != NULL)
        {
            printf("%s='%s' \n", opt->keyword, opt->val);
        }
    }
}
