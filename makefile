PROGRAM=parser-test

SRC = $(shell  find . -type f -name "*.c" -not -path "./build*" -not -path "./cmake*" -not -path "./CMake*")
PARSER_SRC=$(PG_POOL_SRC)/parser
PG_POOL_SRC=../pgpool2/src

OBJ = $(SRC:%.c=%.o)
PG_POOL_OBJS=$(PG_POOL_SRC)/utils/strlcpy.o \
	$(PG_POOL_SRC)/utils/psprintf.o \
	$(PG_POOL_SRC)/parser/libsql-parser.a

PARSER_INCLUDES=./include
PG_POOL_INCLUDES=$(PG_POOL_SRC)/include
POSTGRES_INCLUDES=$(shell pg_config --includedir)

POSTGRES_LIB=$(shell pg_config --libdir)


#ENABLE_GCOV=1

CFLAGS=-Wall -c -g -I$(PARSER_INCLUDES) -I$(PG_POOL_INCLUDES) -I$(POSTGRES_INCLUDES)
CFLAGS+=-O0 -g -DPARSER_TEST
LDFLAGS=-L$(POSTGRES_LIB) -lpq

ifdef ENABLE_GCOV
CFLAGS+= -fprofile-arcs -ftest-coverage
LDFLAGS+= -lgcov
GENHTML_OUTDIR=genhtml
endif

all: deps $(PROGRAM)

deps:
	$(MAKE) -C $(PG_POOL_SRC) utils/strlcpy.o
	$(MAKE) -C $(PG_POOL_SRC) utils/psprintf.o
	$(MAKE) -C $(PG_POOL_SRC)/parser

$(PROGRAM): $(PG_POOL_OBJS) $(OBJ)
	gcc -O0 -g -DPARSER_TEST $(OBJ) $(PG_POOL_OBJS) -o $(PROGRAM) $(LDFLAGS)

%.o: %.c
	gcc -c $(CFLAGS) $< -o $@

#main.o: main.c
#	gcc $(CFLAGS) $<

#utils/print.o: utils/print.c
#	gcc $(CFLAGS) $<

clean: clean-cov
	rm -f $(PROGRAM)
	rm -f **/*.o
	rm -f *.o

.PHONY: all deps test clean-cov clean cov
