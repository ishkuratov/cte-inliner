#include "pool.h"
#include "pool_config.h"
#include "pool_type.h"
#include "context/pool_session_context.h"
#include "utils/elog.h"


POOL_REQUEST_INFO		_req_info;
POOL_REQUEST_INFO *Req_info = &_req_info;
POOL_CONFIG _pool_config;
POOL_CONFIG *pool_config = &_pool_config;
ProcessType processType;


int get_frontend_protocol_version(void) {return 0;}
int set_pg_frontend_blocking(bool blocking) {return 0;}
int send_to_pg_frontend(char* data, int len, bool flush) {return 0;}
int pool_send_to_frontend(char* data, int len, bool flush) {return 0;}
int pool_frontend_exists(void) {return 0;}

// copies from: utils/error/assert.c
void ExceptionalCondition(const char *conditionName, const char *errorType,
                          const char *fileName, int lineNumber) {
    if (!PointerIsValid(conditionName)
        || !PointerIsValid(fileName)
        || !PointerIsValid(errorType))
        write_stderr("TRAP: ExceptionalCondition: bad arguments\n");
    else
    {
        write_stderr("TRAP: %s(\"%s\", File: \"%s\", Line: %d)\n",
                     errorType, conditionName,
                     fileName, lineNumber);
    }

    /* Usually this shouldn't be needed, but make sure the msg went out */
    fflush(stderr);

    abort();
}

POOL_SESSION_CONTEXT *pool_get_session_context(bool noerror) {return NULL;}
int pool_virtual_master_db_node_id(void) {return 0;}
