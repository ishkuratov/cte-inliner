#include "pgpool_stub.h"
#include "utils/palloc.h"
#include "utils/memutils.h"
#include "utils/elog.h"
#include "parser/parser.h"

#include "libpq-fe.h"

#include "utils/print.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>


const PQprintOpt DEFAULT_PRINT_OPT = {
    1,
    1
};

enum {
    TEST_CONTINUE,
    TEST_QUIT
};

static int
command(const char *cmd)
{
    if (*cmd == 'q')
        return TEST_QUIT;
    return TEST_CONTINUE;
}

static void
process_input()
{
    List *tree;
    ListCell *l;
    int		state = TEST_CONTINUE;
    int		notty = (!isatty(fileno(stdin)) || !isatty(fileno(stdout)));
    char	line[1024];


    while (state != TEST_QUIT)
    {
        if (!notty)
            fprintf(stdout, "> ");

        if (!fgets(line, 1024, stdin))
            break;

        *(strchr(line, (int) '\n')) = '\0';

        if (line[0] == '#' || line[0] == '\0')
            continue;

        if (line[0] == '\\')
        {
            state = command(line + 1);
            continue;
        }

        bool error;
        tree = raw_parser(line, &error);

        if (tree == NULL || error)
        {
            printf("syntax error: %s\n", line);
        }
        else
        {
            foreach(l, tree)
            {
                Node *node = (Node *) lfirst(l);
                printf("%s\n", nodeToString(node));
            }
        }
    }
}

static void
exit_nicely(PGconn *conn)
{
    PQfinish(conn);
    exit(1);
}

static void
query(PGconn *conn)
{
    PGresult   *res;
    const char *paramValues[1];

    /*
     * The point of this program is to illustrate use of PQexecParams() with
     * out-of-line parameters, as well as binary transmission of data.
     *
     * This first example transmits the parameters as text, but receives the
     * results in binary format.  By using out-of-line parameters we can
     * avoid a lot of tedious mucking about with quoting and escaping, even
     * though the data is text.  Notice how we don't have to do anything
     * special with the quote mark in the parameter value.
     */

    /* Here is our out-of-line parameter value */
    paramValues[0] = "joe's place";

    res = PQexecParams(conn,
                       "SELECT * FROM test1 WHERE t = $1",
                       1,       /* one param */
                       NULL,    /* let the backend deduce param type */
                       paramValues,
                       NULL,    /* don't need param lengths since text */
                       NULL,    /* default to all text params */
                       0);      /* ask for binary results */

    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        fprintf(stderr, "SELECT failed: %s", PQerrorMessage(conn));
        PQclear(res);
        exit_nicely(conn);
    }

    PQprint(stdout, res, &DEFAULT_PRINT_OPT);
    // TODO: get cost and time.

    PQclear(res);
}


int main(int argc, char **argv)
{
    MemoryContextInit();

    const char *conninfo;
    PGconn     *conn;

    /*
     * If the user supplies a parameter on the command line, use it as the
     * conninfo string; otherwise default to setting dbname=postgres and using
     * environment variables or defaults for all other connection parameters.
     */
    if (argc > 1)
        conninfo = argv[1];
    else
        conninfo = "dbname = postgres";

    /* Make a connection to the database */
    conn = PQconnectdb(conninfo);

    print_pgconn_options(conn);

    /* Check to see that the backend connection was successfully made */
    if (PQstatus(conn) != CONNECTION_OK)
    {
        fprintf(stderr, "Connection to database failed: %s",
                PQerrorMessage(conn));
        exit_nicely(conn);
    }

    process_input();

    /* close the connection to the database and cleanup */
    PQfinish(conn);


    return 0;
}
